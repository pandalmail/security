package com.apukhtin.security.lecture1;

import java.util.stream.Collectors;

import static com.apukhtin.security.common.Utils.ALPHABET;

/**
 * Class represents Caesar cipher algorithm.
 * Applicable for russian language only
 *
 * @author Vlad
 */
public class CaesarCipher {

    public static void main(String[] args) {
        CaesarCipher cipher = new CaesarCipher();
        final int key = 31;
        final String message = "Привет мир!";

        String encoded = cipher.encode(message, key);
        String decoded = cipher.decode(encoded, key);

        System.out.printf("encoded(%s) with key %d = %s %n", message, key, encoded);
        System.out.printf("decoded(%s) with key %d = %s %n", encoded, key, decoded);

    }

    public String encode(String input, int key) {
        return shift(input, key);
    }

    public String decode(String input, int key) {
        return shift(input, -key);
    }

    private String shift(String input, int key) {
        return input
                .toLowerCase()
                .chars()
                .boxed()
                .map(character -> String.valueOf((char) character.intValue()))
                .map(character -> isLetter(character) ?
                        shiftChar(character, key) : character
                )
                .collect(Collectors.joining());
    }

    static String shiftChar(String character, int key) {
        return ALPHABET.get(
                (ALPHABET.indexOf(character) + key + ALPHABET.size()) % ALPHABET.size());
    }

    private boolean isLetter(String string) {
        if (string != null && !string.isEmpty()) {
            return Character.isLetter(string.charAt(0));
        } else {
            return false;
        }
    }
}
