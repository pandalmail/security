package com.apukhtin.security.lecture1;

import java.io.*;
import java.util.LinkedHashMap;
import java.util.Map;

import static com.apukhtin.security.lecture1.Statistics.byOccurrence;
import static com.apukhtin.security.lecture1.Statistics.statisticsFor;

/**
 * Реализовать подсчет статистик криптограмм, полученных методо Цезаря
 * Атоматизировать криптоанализ таких шифрограмм.
 *
 * @author Vlad
 */
public class CaesarStatistics {

    private static final String ART_FILE = "art.txt";
    private static final String ENCODING = "utf-8";
    private static final String CRYPTANALYSIS_OUT = "cryptanalysis.result.txt";

    public static void main(String[] args) throws IOException {
        try (Reader art = new InputStreamReader(new FileInputStream(ART_FILE), ENCODING)) {
            CaesarCipher caesarCipher = new CaesarCipher();
            final int key = 10;

            String text = readerToString(art);
            String encodedText = caesarCipher.encode(text, key);

            System.out.println("Caesar encoded text Statistics: " + byOccurrence(statisticsFor(encodedText)));
            System.out.println("Brutforce cryptanalysis...");
            try (BufferedWriter writer = new BufferedWriter(new FileWriter(CRYPTANALYSIS_OUT))) {
                Map<Integer, String> assumptions = brutforce(text);
                assumptions.forEach((assumptionKey, assumptionText) -> {
                    try {
                        writer.write(String.format("\t%d:%s %n", assumptionKey, assumptionText));
                    } catch (IOException e) {
                        throw new RuntimeException(e);
                    }
                });
            }
            System.out.println("Brutforce cryptanalysis saved to " + CRYPTANALYSIS_OUT);
        }
    }

    private static String readerToString(Reader reader) throws IOException {
        try (BufferedReader bufferedReader = new BufferedReader(reader);) {
            StringBuilder result = new StringBuilder();
            String line = null;
            while ((line = bufferedReader.readLine()) != null) {
                result.append(line);
            }
            return result.toString();
        }
    }

    public static Map<Integer, String> brutforce(String input) {
        Map<Integer, String> assumptions = new LinkedHashMap<>();
        CaesarCipher caesarCipher = new CaesarCipher();

        for (int i = 0; i < 33; i++) {
            assumptions.put(i, caesarCipher.decode(input, i));
        }

        return assumptions;
    }

}
