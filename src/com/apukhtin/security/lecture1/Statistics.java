package com.apukhtin.security.lecture1;

import java.io.*;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.stream.Stream;

/**
 * Реализовать подсчет статистики встречаемости букв. Составить шаблоны для текстов
 * художественных, технич. документации, сказок
 *
 * @author Vlad
 */
public class Statistics {

    private static final String ENCODING = "utf-8";

    private static final String ART_FILE = "art.txt";
    private static final String DOCUMENTATION_FILE = "documentation.txt";
    private static final String TALE_FILE = "tale.txt";

    static Map<Character, Integer> statisticsFor(Reader reader) throws IOException {
        Map<Character, Integer> statistics = new HashMap<>();
        try (BufferedReader bufferedReader = new BufferedReader(reader)) {
            String line = null;
            while ((line = bufferedReader.readLine()) != null) {
                addLineStatistics(line, statistics);
            }
        }
        return statistics;
    }

    static Map<Character, Integer> statisticsFor(String string) throws IOException {
        Map<Character, Integer> statistics = new HashMap<>();
        addLineStatistics(string, statistics);
        return statistics;
    }

    public static void main(String[] args) throws IOException {
        try(
                Reader art = new InputStreamReader(new FileInputStream(ART_FILE), ENCODING);
                Reader docs = new InputStreamReader(new FileInputStream(DOCUMENTATION_FILE), ENCODING);
                Reader tale = new InputStreamReader(new FileInputStream(TALE_FILE), ENCODING);
        ) {
            System.out.printf("Art text statistics:%n%s%n", byOccurrence(statisticsFor(art)));
            System.out.printf("Documentation text statistics:%n%s%n", byOccurrence(statisticsFor(docs)));
            System.out.printf("Fairy tale text statistics:%n%s%n", byOccurrence(statisticsFor(tale)));
        }
    }

    private static void addLineStatistics(String line, Map<Character, Integer> statistics) {
        final int defaultValue = 1;

        line
                .chars()
                .filter(Character::isLetter)
                .map(Character::toLowerCase)
                .forEach(character -> {
                    statistics.merge((char) character, defaultValue, (oldValue, defaultOne) -> oldValue + 1);
                });
    }

    static Map<Character, Integer> byOccurrence(Map<Character, Integer> statistics) {
        Map<Character, Integer> result = new LinkedHashMap<>();
        Stream<Map.Entry<Character, Integer>> sortedEntries =
                statistics
                        .entrySet()
                        .stream()
                        .sorted((oneEntry, anotherEntry) -> Integer.compare(anotherEntry.getValue(), oneEntry.getValue()));
        ;
        sortedEntries.forEach(entry -> result.put(entry.getKey(), entry.getValue()));
        return result;
    }

}
