package com.apukhtin.security.practical1;

import java.util.*;
import java.util.stream.Collectors;

import static com.apukhtin.security.common.Utils.ALPHABET;
import static com.apukhtin.security.common.Utils.isLetter;

/**
 * Practical lesson 1
 * Implemented Monoalphabetic ciphering by map:
 * key = character:  value = another character.
 * Note: another character mapping is generated randomly.
 * Every characters should have different shift steps, which is implemented by
 * random character pick.
 *
 * @author Vlad
 * @see MonoalphabeticCipher#generateKey()
 */
public class MonoalphabeticCipher {

    public static void main(String[] args) {
        MonoalphabeticCipher cipher = new MonoalphabeticCipher();
        Map<String, String> key = generateKey();

        String message = "привет, мир!";
        String encoded = cipher.encode(message, key);
        String decoded = cipher.decode(encoded, key);

        System.out.println(key);
        System.out.println(message);
        System.out.println(encoded);
        System.out.println(decoded);
    }

    public static Map<String, String> generateKey() {
        List<String> availableChars = new ArrayList<>(ALPHABET);
        Map<String, String> key = new HashMap<>();
        Random random = new Random();

        for (String character : ALPHABET) {
            int randomCharIndex = random.nextInt(availableChars.size());
            String shiftedChar = availableChars.get(randomCharIndex);
            availableChars.remove(shiftedChar);

            key.put(character, shiftedChar);
        }

        return key;
    }

    public String encode(String message, Map<String, String> key) {
        StringBuilder encoded = new StringBuilder();
        char[] messageChars = message.toCharArray();
        for (int i = 0; i < message.toCharArray().length; i++) {
            String character = String.valueOf(messageChars[i]);
            encoded.append(
                    isLetter(character) ?
                            key.get(character)
                            : character
            );
        }

        return encoded.toString();
    }

    public String decode(String message, Map<String, String> key) {
        key = swapMap(key);
        return encode(message, key);
    }

    /**
     * swap map keys and values to decode encoded value
     */
    private Map<String, String> swapMap(Map<String, String> map) {
        return map
                .entrySet()
                .stream()
                .collect(Collectors.toMap(Map.Entry::getValue, Map.Entry::getKey));
    }

}
