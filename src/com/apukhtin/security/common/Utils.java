package com.apukhtin.security.common;

import java.util.Arrays;
import java.util.List;

/**
 * @author Vlad
 */
public class Utils {

    public static final List<String> ALPHABET =
            Arrays.asList(
                    "а, б, в, г, д, е, ё, ж, з, и, й, к, л, м, н, о, п, р, с, т, у, ф, х, ц, ч, ш, щ, ъ, ы, ь, э, ю, я"
                            .split(", ")
            );

    public static boolean isLetter(String string) {
        if (string != null && !string.isEmpty()) {
            return Character.isLetter(string.charAt(0));
        } else {
            return false;
        }
    }

    public static boolean isLetter(Character character) {
        return Character.isLetter(character);
    }

}
